import datetime
import os
import discord
from discord.ext import commands

class FoundationBot(commands.Bot):
    def __init__(self):
        super().__init__(
            command_prefix='::',
            intents = discord.Intents.default(),
            application_id = 994625094531956736)
        
    async def setup_hook(self):
        await self.load_extension("cogs.serverstartup")
        await bot.tree.sync(guild = discord.Object(id = 671017986693398538)) # guildid

    async def on_ready(self):
        now = datetime.datetime.now()
        print(now, " ", "Connected")


bot = FoundationBot()
token = os.environ.get("TOKEN")
bot.run(token)

