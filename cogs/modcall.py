import datetime
import discord
from discord.ui import Button, View
from discord import app_commands
from discord.ext import commands

class modcall(commands.Cog):
    def __init__(self, bot: commands.Bot) -> None:
            self.bot = bot

    @app_commands.command(
        name = "modcall",
        description = "Ping Server Moderator"
    )

    async def pingmod(
        self,
        interaction: discord.Integration) -> None:

        async def agreebutton_callback(interaction):
            await interaction.response.edit_message(view=None)
            channel = self.bot.get_channel(804381395584680006)
            await channel.send("<@&1015356595011600435>")

        embed = discord.Embed(
            title = "Are you sure?",
            description = """
            Using this command without a valid reason will result in punishment. The command should be used for emergencies only, this includes (but is not limited to): 
                
                - Server raid
                - Mass NSFW
                - Mass spam/Chat flood

            Are you sure you want to use this command?
            """
        )
        embed.set_author(name="Foundation", icon_url="https://cdn.discordapp.com/attachments/720653649906237560/1038439300724961400/uh_2.png")
        embed.set_footer(text=f"Moderation Team", icon_url="https://cdn.discordapp.com/attachments/720653649906237560/1038439300724961400/uh_2.png")

        agreebutton = Button(label="Continue", style=discord.ButtonStyle.blurple)
        cancelbutton = Button(label="Cancel", style=discord.ButtonStyle.red)

        agreebutton.callback = agreebutton_callback

        buttonlist = View()
        buttonlist.add_item(agreebutton)
        buttonlist.add_item(cancelbutton)

        await interaction.response.send_message(view=buttonlist, embed=embed, ephemeral = True, )