import datetime
import requests
import json
import discord
import asyncio
from discord import app_commands
from discord.ext import commands

from cogs.modcall import modcall

class serverstartup(commands.Cog):
    def __init__(self, bot: commands.Bot) -> None:
        self.bot = bot

    @app_commands.command(
        name = "ssu",
        description = "Server Start Up"
    )

    @app_commands.choices(choices=[
        app_commands.Choice(name="Official", value="OFFICIAL"),
        app_commands.Choice(name="Unofficial", value="UNOFFICIAL"),
        ])

    async def ssu(
        self,
        interaction: discord.Integration,
        choices: app_commands.Choice[str],
        mention: discord.Role,
        message: str) -> None:

        await interaction.response.send_message("Started!", ephemeral = True)
        channel = self.bot.get_channel(1072256592717807659)
        author = interaction.user.name
        
        embed = discord.Embed(
            title = f"{choices.value} SSU",
            timestamp = datetime.datetime.utcnow(),
            description = f"{message} \n https://www.roblox.com/games/10713125056/ARF-156"
        )
        embed.set_thumbnail(url="https://cdn.discordapp.com/attachments/720653649906237560/1038439300724961400/uh_2.png")
        embed.set_author(name="Foundation", icon_url="https://cdn.discordapp.com/attachments/720653649906237560/1038439300724961400/uh_2.png")
        embed.set_footer(text=f"Started by {author}", icon_url=interaction.user.avatar.url)
        
        if (mention is discord.Role.is_default):
            sendembed = await channel.send("@everyone", embed=embed)
        else:
            sendembed = await channel.send(mention.mention, embed=embed)
        ended = False

        await asyncio.sleep(120) # wait for player to join first
        while ended == False:
            gamedata = requests.get('https://games.roblox.com/v1/games/10713125056/servers/public?sortOrder=Asc&limit=100')
            data = json.loads(gamedata.text)
            serverlist = data['data']
            if serverlist == []:
                ended = True
                embed.title = f"ENDED, {choices.value} SSU"
                await sendembed.edit(embed=embed)
                return
            await asyncio.sleep(300)

async def setup(bot: commands.Bot) -> None:
    await bot.add_cog(
        serverstartup(bot),
        guilds = [discord.Object(id = 671017986693398538)]    
    )

    await bot.add_cog(
        modcall(bot),
        guilds = [discord.Object(id = 671017986693398538)]   
    )