FROM python:3.10.8-alpine3.16

ENV PYTHONDONTWRITEBYTECODE=1

ENV PYTHONUNBUFFERED=1

RUN apk -U upgrade
RUN apk add git

RUN python -m pip install requests
RUN python -m pip install asyncio
RUN python -m pip install git+https://github.com/rapptz/discord.py@master

WORKDIR /app
COPY . /app

RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

CMD ["python", "main.py"]